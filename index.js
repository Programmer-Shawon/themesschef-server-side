// variable
const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 5000;
app.use(cors());


// get data
const allChef = require('./data/allchef.json')

// routes
app.get('/', (req, res) => {
    res.send('server running ',)
})
// all chef data
app.get('/all-chef', (req, res) => {
    res.send(allChef)
})
// chef data by id
app.get('/all-chef/chef/:chefId', (req, res) => {
    const id = req.params.chefId;
    const chefDetails = allChef.data.filter((data) => data.id === id)
    res.send(chefDetails);
})

app.get('*', (req, res) => {
    res.send('Wrong Api.')
})

app.listen(port, () => {
    console.log(`server running port : ${port}`);
})
